﻿using PDF.source.controller.pdf;
using PDF.source.controller.bytes;
using System.Drawing;
using System;

namespace PDF.view
{
    class Program
    {
        private static string[] words = { @"D:\Z\", "C# Pdf Test LL", "Header test." };
        private static Image[] images = { Image.FromFile(@"D:\Project\Shark\Dev-MM\PDF\image\logo.jpg"),
            Image.FromFile(@"D:\Project\Shark\Dev-MM\PDF\image\1.gif"),
            Image.FromFile(@"D:\Project\Shark\Dev-MM\PDF\image\2.gif"),
            Image.FromFile(@"D:\Project\Shark\Dev-MM\PDF\image\3.gif")
        };

        static void Main(string[] args)
        {
            PdfGenerator pdf = new PdfGenerator();

            PdfByte.ByteToPdfDisc(
                pdf.Generate(words[2], ImageByte.ImageToByteArray(images[0]), ImageByte.ImageToByteArray(images[1]),
                            ImageByte.ImageToByteArray(images[2]), ImageByte.ImageToByteArray(images[3])
                ), words[0], words[1]
            );

            Console.WriteLine(string.Format("Utworzono plik PDF.\nNazwa: {1}.pdf\nLokacja: {0}\nPelna sciezka: {0}{1}.pdf", words[0], words[1]));
            Console.ReadLine();
        }
    }
}