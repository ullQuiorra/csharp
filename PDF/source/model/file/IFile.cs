﻿using iTextSharp.text;
using System.IO;

namespace PDF.source.model.file
{
#region Interface of files.
    interface IFile
    {
        #region Create file.
        void Create();
        #endregion
        #region Open file for actions.
        void Open();
        #endregion
        #region Close file document.
        void Close();
        #endregion
        #region Current document.
        Document Document();
        #endregion
        #region MemoryStream of current file.
        MemoryStream MemoryStream();
        #endregion
        #region Add page to file.
        void AddPage();
        #endregion
        #region Add pages to file.
        void AddPages(int valueOfPages);
        #endregion
        #region Add text to file.
        void AddText(string text);
        #endregion
        #region Add IElement to file.
        void Add(IElement element);
        #endregion
    }
#endregion
}
