﻿using System.IO;
using iTextSharp.text;

namespace PDF.source.model.file
{
#region Abstract class of file.
    abstract class File : IFile
    {
        private static Document document;
        private static MemoryStream memoryStream;
        private static string _fileFormat;
        protected static string FileName { set; get; }      
        protected static string FileFormat
        {
            private set
            {
                char[] text = value.ToCharArray();
                if (text[0] != '.')
                {
                    _fileFormat = ('.' + value).ToLower();
                }
                else
                {
                    _fileFormat = value.ToLower();
                }

            }
            get
            {
                return _fileFormat;
            }
        }

    #region Constructors.
        #region Default constructor for file with specific name and format.
        public File(string name, string format)
        {
            InitializeFile(name, format);
        }
        #endregion
    #endregion

    #region Methods
        #region Open document for actions.
        public void Open()
        {
            document.Open();
        }
        #endregion
        #region Close file.
        public void Close()
        {
            if(document.IsOpen())
            {
                document.Close();
            }
            else { return; }
        }
        #endregion
        #region Current file.
        public Document Document()
        {
            return document;
        }
        #endregion
        #region Current memorystream of file.
        public MemoryStream MemoryStream()
        {
            return memoryStream;
        }
        #endregion
        #region Method set details for file create.
        private static void InitializeFile(string name, string format)
        {
            FileName = name;
            FileFormat = format;
            document = new Document(PageSize.A4, 10f, 10f, 60f, 30f);
            memoryStream = new MemoryStream();
        }
        #endregion
        #region Create file overrloaded.
        virtual public void Create() { }
        #endregion
        #region Add Ielement to file.
        public void Add(IElement element)
        {
            document.Add(element);
        }
        #endregion
        #region Add page to PDF.
        public void AddPage()
        {
            document.NewPage();
        }
        #endregion
        #region Add pages to file.
        public void AddPages(int valueOfPages)
        {
            if (valueOfPages != 0)
            {
                do
                {
                    document.NewPage();
                    valueOfPages--;
                }
                while (valueOfPages == 0);
            }
            else { return; }
        }
        #endregion
        #region Add text to file.
        public void AddText(string text)
        {
            if (text != null)
            {
                document.Add(new Paragraph(text));
            }
            else { return; }
        }
        #endregion
    #endregion
    }
#endregion
}
