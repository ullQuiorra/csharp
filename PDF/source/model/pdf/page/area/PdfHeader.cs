﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using PDF.source.controller.bytes;

namespace PDF.source.model.pdf.area
{
#region Class to create header of pdf.
    class PdfHeader
    {
        private const int textSize = 10;

        #region General constructor to create header of pdf.
        public PdfHeader(PdfWriter writer, Document document, string headerText, byte[] imageByteArray, PdfContentByte contentByte)
        {
            HeaderPdfInitialize(writer, document, headerText, imageByteArray, contentByte);
        }
        #endregion
        #region Create text for header with specific font.
        private static Phrase TextOfHeader(string headerText)
        {
            return new Phrase(headerText, FontFactory.GetFont("verdana", textSize)); 
        }
        #endregion
        #region Create image for header from array byte.
        private static iTextSharp.text.Image ImageOfHeader(byte[] byteArray)
        {
            System.Drawing.Bitmap bitmapOfConvertedImage = new System.Drawing.Bitmap(ImageByte.ArrayByteToImage(byteArray));
            return iTextSharp.text.Image.GetInstance(bitmapOfConvertedImage, System.Drawing.Imaging.ImageFormat.Jpeg);
        }
        #endregion
        #region Create table of header with 2 columns for image and text.
        private static PdfPTable TableOfHeader(Document document, iTextSharp.text.Image imageOfHeader, Phrase textOfHeader)
        {
            PdfPTable table = new PdfPTable(2);
            PdfPCell cellOfImage = new PdfPCell(imageOfHeader);
            PdfPCell cellOfText = new PdfPCell(textOfHeader);
            
            cellOfImage.HorizontalAlignment = Element.ALIGN_LEFT;
            cellOfImage.Border = 0;
            cellOfText.HorizontalAlignment = Element.ALIGN_RIGHT;
            cellOfText.Border = 0;
            table.AddCell(cellOfImage);
            table.AddCell(cellOfText);
            table.TotalWidth = document.PageSize.Width - 80;

            return table;
        }
        #endregion
        #region Create full pdf header.
        private static void HeaderPdfInitialize(PdfWriter writer, Document document, string headerText, byte[] byteArray, PdfContentByte contentByte)
        {
            Phrase text = TextOfHeader(headerText);
            iTextSharp.text.Image image = ImageOfHeader(byteArray);

            TableOfHeader(document, image, text).WriteSelectedRows(0, -1, 10, document.PageSize.Height - 10, writer.DirectContent);
            contentByte = writer.DirectContent;
            contentByte.LineTo(document.PageSize.Width - 40, document.PageSize.Height - 35);
            contentByte.Stroke();
        }
        #endregion
    }
#endregion
}
