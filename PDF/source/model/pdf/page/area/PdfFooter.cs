﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;

namespace PDF.source.model.pdf.area
{
#region Class for footer of pdf.
    class PdfFooter
    {
        private const int bottom = 15;
        private const int right = 15;
        private const int left = right;
        private const int textSize = 8;

        #region General constructor to create footer of pdf.
        public PdfFooter(PdfWriter writer, Document document, PdfTemplate template, PdfContentByte contentByte, BaseFont baseFont, DateTime PrintTime)
        {
            FooterPdfInitialize(writer, document, template, contentByte, baseFont, PrintTime);
        }
        #endregion
        #region Create full footer for pdf.
        private static void FooterPdfInitialize(PdfWriter writer, Document document, PdfTemplate template, PdfContentByte contentByte, BaseFont baseFont, DateTime PrintTime)
        {
            int pageNumber = writer.PageNumber;
            string footerText = string.Format("Strona {0}{1}", pageNumber, " z ");
            float lenght = baseFont.GetWidthPoint(footerText, textSize);
            Rectangle pageSize = document.PageSize;

            contentByte.SetRGBColorFill(100, 100, 100);
            contentByte.BeginText();
            contentByte.SetFontAndSize(baseFont, textSize);
            contentByte.SetTextMatrix(pageSize.GetLeft(left), pageSize.GetBottom(bottom));
            contentByte.ShowText(footerText); 
            contentByte.EndText();
            contentByte.AddTemplate(template, pageSize.GetLeft(left) + lenght, pageSize.GetBottom(bottom));

            contentByte.BeginText();
            contentByte.SetFontAndSize(baseFont, textSize);
            contentByte.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, string.Format("Utworzony: {0}", PrintTime.ToString()), pageSize.GetRight(right), pageSize.GetBottom(bottom), 0);
            contentByte.EndText();
        }
        #endregion
    }
#endregion
}
