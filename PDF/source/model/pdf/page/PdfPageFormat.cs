﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using PDF.source.model.pdf.area;
using System;

namespace PDF.source.model.pdf.page
{
#region Class to create header and footer for PDF.
    class PdfPageFormat : PdfPageEventHelper
    {
        private static DateTime PrintTime = DateTime.Now;
        private static PdfContentByte contentByte; 
        private static PdfTemplate template; 
        private static BaseFont baseFont = null;
        public string HeaderText { set; private get; }
        public byte[] HeaderImageArrayByte { set; private get; }

    #region constructors
        #region Constructor with base footer and specific header text with image.
        public PdfPageFormat(string headerText, byte[] headerImageArrayByte)
        {
            HeaderText = headerText;
            HeaderImageArrayByte = headerImageArrayByte;
        }
        #endregion
    #endregion

    #region Methods
        #region Override the onOpenDocument method to set base attributes of document.
        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            try
            {
                PrintTime = DateTime.Now; 
                baseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                contentByte = writer.DirectContent;
                template = contentByte.CreateTemplate(50, 50);
            }
            catch (DocumentException de)
            {
                Console.WriteLine("||On OpenDocument||" + de.StackTrace);
            }
            catch (System.IO.IOException ioe)
            {
                Console.WriteLine("||On OpenDocument||" + ioe.StackTrace);
            }
        }
        #endregion
        #region Override the onStartPage method to set Header.
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
            PdfHeader header = new PdfHeader(writer, document, HeaderText, HeaderImageArrayByte, contentByte);


        }
        #endregion
        #region Override the onEndPage method to set Footer.
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);
            PdfFooter footerBegin = new PdfFooter(writer, document, template, contentByte, baseFont, PrintTime);
            Rectangle pageBorder = new Rectangle(document.PageSize);

            pageBorder.Left += (document.LeftMargin - 1);
            pageBorder.Right -= (document.RightMargin - 1);
            pageBorder.Top -= (document.TopMargin - 50);
            pageBorder.Bottom += (document.BottomMargin - 5);
            contentByte.SetColorStroke(BaseColor.BLACK);
            contentByte.Rectangle(pageBorder.Left, pageBorder.Bottom, pageBorder.Width, pageBorder.Height);
            contentByte.Stroke();
        }
        #endregion
        #region Override the onCloseDocument method to set page numbering.
        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);
            int lastPageNumber = writer.PageNumber - 1;

            template.BeginText();
            template.SetFontAndSize(baseFont, 8);
            template.SetTextMatrix(0, 0);
            template.ShowText(lastPageNumber + ".");
            template.EndText();
        }
        #endregion
        #endregion
    }
#endregion
}