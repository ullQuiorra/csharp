﻿using PDF.source.model.file;

namespace PDF.source.model.pdf
{
#region Pdf interface.
    interface IPdf : IFile
    {
        #region Get page number.
        int PageNumber();
        #endregion
        #region Set image and/or text for header.
        void HeaderOfPdf(byte[] imageArrayByte, string headerText);
        #endregion
    }
#endregion
}
