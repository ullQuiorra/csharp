﻿using iTextSharp.text.pdf;
using PDF.source.model.file;
using PDF.source.model.pdf.page;

namespace PDF.source.model.pdf
{
#region Pdf base class needed to generate pdf document. 
    class Pdf : File, IPdf
    {
        private const string format = "pdf";
        private static string TextHeader { set; get; }
        private static byte[] ImageArrayByte { set; get; }
        private static PdfWriter writer;

    #region Constructors
        #region Default PDF constructor with specific name of PDF.
        public Pdf(string fileName) : base(fileName, format)
        {
            FileName = string.Format("{0}{1}", fileName, format);
        }
        #endregion
    #endregion

    #region Methods
        #region Get page number.
        public int PageNumber()
        {
            return writer.PageNumber;
        }
        #endregion      
        #region Set detail data of pdf header.
        public void HeaderOfPdf(byte[] imageArrayByte, string headerText)
        {
            ImageArrayByte = imageArrayByte;
            TextHeader = headerText;
        }
        #endregion
        #region Create pdf document.
        public override void Create()
        {
            writer = PdfWriter.GetInstance(Document(), MemoryStream());
            writer.CloseStream = false;
            writer.PageEvent = new PdfPageFormat(TextHeader, ImageArrayByte);
        }
        #endregion
    #endregion
    }
#endregion
}
