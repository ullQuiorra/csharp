﻿using PDF.source.model.pdf;
using PDF.source.controller.bytes;

namespace PDF.source.controller.pdf
{
#region Generator of specific PDF.
    class PdfGenerator
    {
        private static IPdf pdf = null;
        public string PDF_NAME { set; private get; } 
        public string HEADER_TEXT { set; private get; }
        public string IMAGE_PATH { set; private get; }

        #region Defautl constructor to create PDF file with specific name and path.
        public PdfGenerator()
        {
            IMAGE_PATH = null;
            PDF_NAME = null;
            HEADER_TEXT = null;
        }
        #endregion

        #region Settings for first generated page of PDF.
        private static void firstPage()
        {
            pdf.AddText( string.Format("[S]trona {0}", pdf.PageNumber()) );
            pdf.AddText("Wygenerowana strona. Test.");
            pdf.AddText(string.Format("Koniec generowanej {0} strony.", pdf.PageNumber()) );
        }
        #endregion
        #region Generate current PDF with special header.
        public byte[] Generate(string headerText, byte[] headerImage, params byte[][] images)
        {
            pdf = new Pdf(PDF_NAME);
            pdf.HeaderOfPdf(headerImage, headerText);
            
            pdf.Create();
            pdf.Open();
            {
                if (images != null)
                {
                    firstPage();
                    foreach (byte[] image in images)
                    {
                        pdf.AddPage();
                        pdf.AddText(string.Format("[S]trona {0}.", pdf.PageNumber()));
                        pdf.AddText("Wygenerowana strona. Test.");
                        pdf.AddText(string.Format("Koniec generowanej {0} strony.", pdf.PageNumber()));
                        pdf.Add(ImageByte.ArrayByteToImageSharp(image));
                    }
                    pdf.AddPage();
                    pdf.AddText(string.Format("Ostatnia strona z wszystkimi obrazkami."));
                    foreach (byte[] image in images)
                    {
                        pdf.Add(ImageByte.ArrayByteToImageSharp(image));
                    }
                }
            }
            pdf.Close();

            return pdf.MemoryStream().ToArray();
        }
        #endregion
    }
#endregion
}
