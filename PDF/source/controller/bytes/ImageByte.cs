﻿using System.IO;

namespace PDF.source.controller.bytes
{
#region Class for convert images to array byte.
    class ImageByte
    {
        #region Convert image to bytearray
        public static byte[] ImageToByteArray(System.Drawing.Image image)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                image.Save(memoryStream, image.RawFormat);
                return memoryStream.ToArray();
            }
        }
        #endregion
        #region Convert bytearray to image
        public static System.Drawing.Image ArrayByteToImage(byte[] arrayByte)
        {
            using (MemoryStream memoryStream = new MemoryStream(arrayByte))
            {
                return System.Drawing.Image.FromStream(memoryStream);
            }
        }
        public static iTextSharp.text.Image ArrayByteToImageSharp(byte[] arrayByte)
        {
            using (MemoryStream memoryStream = new MemoryStream(arrayByte))
            {
                return iTextSharp.text.Image.GetInstance(memoryStream);
            }
        }
        #endregion
    }
#endregion
}