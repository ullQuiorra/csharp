﻿using System.IO;
using System.Text;

namespace PDF.source.controller.bytes
{
#region Class for converting PDF with arraybyte.
    class PdfByte
    {
        #region Converty pdf from arraybyte to file PDF on disc.
        public static void ByteToPdfDisc(byte[] pdfByte, string path, string name)
        {
            using (FileStream fileStream = new FileStream(string.Format("{0}{1}{2}", path, name, ".pdf"), FileMode.CreateNew))
            {
                using (StreamWriter streamWriter = new StreamWriter(fileStream, Encoding.UTF8))
                {
                    fileStream.Write(pdfByte, 0, pdfByte.Length);
                    streamWriter.Close();
                }
            }
        }
        #endregion
    }
    #endregion
}
